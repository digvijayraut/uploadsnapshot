#include <iostream>
#include <curl/curl.h>
  #include <unistd.h>
#include <stdio.h>
#include <ctime>
#include <sstream>
#include <thread>
#include <iomanip>
#include <vector>
#include <fstream>
//#include <boost/date_time.hpp>
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
//namespace bpt = boost::posix_time;
using namespace rapidjson;
using namespace std;

string macAddress="";
vector<string> activeChannelsNo;

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  stringstream ss(str); // Turn the string into a stream.
  string tok;
 
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
 
  return internal;
}
 

size_t callbackMacAddress(
            char* in,
            std::size_t size,
            std::size_t num,
            std::string* out)
    {
		const std::size_t totalBytes(size * num);
		out->append(in, totalBytes);
		//cout << "Value of out" << in << endl;
		string delimiter = "table.Network";
		string myCSV(in);
		myCSV.erase(0, myCSV.find(delimiter) + delimiter.length());
		cout << myCSV <<endl;
		size_t pos = 0;
		string token;
		while ((pos = myCSV.find(delimiter)) != string::npos) {
		    token = myCSV.substr(0, pos);
		    cout << token << endl;
			vector<string> sep = split(token, '=');	
			for(int i = 0; i < sep.size(); ++i) {
				if (".eth0.PhysicalAddress" == sep[i]) {
	
				    macAddress = sep[i+1];
				}
			}
    			myCSV.erase(0, pos + delimiter.length());
		}
		 
        return totalBytes;
    }

size_t callback(
            char* in,
            std::size_t size,
            std::size_t num,
            std::string* out)
    {
		const std::size_t totalBytes(size * num);
		out->append(in, totalBytes);
		//cout << "Value of out" << in << endl;
		string delimiter = "table.VideoWidget[";
		string myCSV(in);
		myCSV.erase(0, myCSV.find(delimiter) + delimiter.length());
		//cout << myCSV <<endl;
		size_t pos = 0;
		string token;
		//if (activeChannelsNo.size() > 0)
			//activeChannelsNo.clear();

		while ((pos = myCSV.find(delimiter)) != string::npos) {
		    token = myCSV.substr(0, pos);
		    //cout << token << endl;
			vector<string> sep = split(token, '=');	
			for(int i = 0; i < sep.size(); ++i) {
				string::size_type loc = sep[i].find( "].ChannelTitle.PreviewBlend", 0 );
				if( loc != string::npos ) {
				    cout << "\nValue of sep[i]...." << sep[i];
				    vector<string> channelSplit = split(sep[i], ']');
				    if (channelSplit.size() > 0) {
				    	cout<< "\n Channel No active one.." << channelSplit[0];
					activeChannelsNo.push_back(channelSplit[0]);
				    }
				}
			}
    			myCSV.erase(0, pos + delimiter.length());
		}
		 
        return totalBytes;
    }


void retrieveDeviceMacAddress() {

	//CURL CODE TO FETCH MAC ADDRESS
		CURL *curl;
  		CURLcode res;
  		curl = curl_easy_init();
		if(curl) {
			curl_easy_setopt(curl, CURLOPT_URL, "http://192.168.1.6/cgi-bin/configManager.cgi?action=getConfig&name=Network");
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
			curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
			curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_easy_setopt(curl, CURLOPT_USERPWD, "admin:123456");
			unique_ptr<string> httpData(new string());

			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callbackMacAddress);
			curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
			res = curl_easy_perform(curl);
			cout << "\nMac Address res value.." << res;
			curl_easy_cleanup(curl);
			

			
  			}
		
}
/**
*
*/
void retriveActiveChannels() {

				CURL *curl;
				CURLcode res;
				ostringstream channelURL;
				channelURL << "http://192.168.1.6/cgi-bin/configManager.cgi?action=getConfig&name=VideoWidget";
				cout << "channelURL ..." << channelURL.str() << endl;
				curl = curl_easy_init();
				if(curl) {
					curl_easy_setopt(curl, CURLOPT_URL, channelURL.str().c_str());
					curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
					curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
					curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_easy_setopt(curl, CURLOPT_USERPWD, "admin:123456");
					unique_ptr<string> httpData(new string());
					curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);
					curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());
					res = curl_easy_perform(curl);
					cout << "res..." <<res <<endl;
					curl_easy_cleanup(curl);
		 		}
}

int upload_snapshot(){

	retriveActiveChannels();
	cout << "\nSize of activeChannelsNo vector..." << activeChannelsNo.size();		
	for (int i=0; i<activeChannelsNo.size(); i++)	{
//		cout << "Called at " << bpt::microsec_clock::local_time().time_of_day() << '\n';
		CURL *image;
		CURLcode imgresult;
		FILE *fp;
		ostringstream channelURL;
		channelURL << "http://192.168.1.6/cgi-bin/snapshot.cgi?channel="<<activeChannelsNo[i];
 


				
		char buffer[30];
	  	struct timeval tv;
	  	time_t curtime;
	  	gettimeofday(&tv, NULL); 
	  	curtime=tv.tv_sec;
		strftime(buffer,30,"%Y-%m-%dT%T_",localtime(&curtime));
	  	std::stringstream timestamp;
	  	timestamp<<buffer<<"Z";
		std::string c = timestamp.str();
		std::cout << "\ntime..." << c;

				
		ostringstream oss;
		ostringstream ossS3BucketFileName;
		ostringstream localFilePath;
		std::cout << "\nfinal String ... ";	
		//std::cout << "\nfinal String ... "<< timestamp.str();

		ossS3BucketFileName<< timestamp.str()<<".jpeg";
		localFilePath << "/home/ecom/Upload-Snapshot/"<<i<<"/"<<ossS3BucketFileName.str();	 			
		image = curl_easy_init();
		std::cout << "\nafter curl init ... ";
		 if( image ){
			 
				fp = fopen(localFilePath.str().c_str(), "wb");
			 	if( fp == NULL ) 
					std::cout <<"\nFile cannot be opened";
				curl_easy_setopt(image, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_easy_setopt(image, CURLOPT_USERPWD, "admin:123456");
				curl_easy_setopt(image, CURLOPT_WRITEFUNCTION, NULL);
				curl_easy_setopt(image, CURLOPT_WRITEDATA, fp);
				curl_easy_setopt(image, CURLOPT_URL, channelURL.str().c_str());
		 
			 	imgresult = curl_easy_perform(image);
				 if( imgresult ){
					std::cout << "\nCannot grab the image!";
				 }
				char *localFilePathVal = new char[1024];
				strcpy(localFilePathVal, localFilePath.str().c_str());	

				char *ossS3BucketFileNameVal = new char[1024];
				strcpy(ossS3BucketFileNameVal, ossS3BucketFileName.str().c_str());	

				char *macAddressVal = new char[1024];
				strcpy(macAddressVal, macAddress.c_str());

				char *channelNoVal = new char[1024];
				strcpy(channelNoVal, activeChannelsNo[i].c_str());
				cout << "\nValue of macAddress..>>>"<< macAddress.c_str() <<macAddressVal;
				pid_t pid;
				char *const parmList[] = {"/home/ecom/www/cgi-bin/pushLiveVideoSnapShot.sh", "00:40:7f:97:71:af", 			 					ossS3BucketFileNameVal,channelNoVal,localFilePathVal, NULL};
	
				     if ((pid = fork()) == -1)
					perror("fork error");
				     else if (pid == 0) {
					execv("/home/ecom/www/cgi-bin/pushLiveVideoSnapShot.sh", parmList);
					cout <<"Return not expected. Must be an execv error.n";
				     }
			}

		 			
		 curl_easy_cleanup(image);
		 fclose(fp);
	}
	 return 0;
} 
void caller()
{
    for(;;) {
	activeChannelsNo.clear();
        upload_snapshot();
        std::this_thread::sleep_for(std::chrono::seconds(60));
    }
}

int main()
{
    retrieveDeviceMacAddress();
    std::thread thr(caller);
    thr.join();

	
}
